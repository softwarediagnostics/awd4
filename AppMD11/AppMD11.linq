<Query Kind="Program">
  <Reference Relative="DllMD11\bin\Release\DllMD11.dll">C:\AWD3\AppMD11\DllMD11\bin\Release\DllMD11.dll</Reference>
</Query>

// AppMD11.linq
// Copyright (c) 2013 - 2018 Software Diagnostics Services

void Main()
{
	new ClassMain().Main();
}

public class ClassMain
{
	static string cs1 = "critical section 1";
	static string cs2 = "critical section 2";

	static void DoWork()
	{
		Thread.Sleep(100);
	}	

	static void thread_proc_1()
	{
		Monitor.Enter(cs1);
		{	
			DoWork();
		}
		Monitor.Exit(cs1);
		
		Thread.Sleep(2000);
		
		Monitor.Enter(cs2);
		{
			DoWork();		
		}
		Monitor.Exit(cs2);		
		
		Console.WriteLine("Thread 1 has finished.");
	}
	
 
	static void thread_proc_2()
	{
		Monitor.Enter(cs2);
		{
			DoWork();
			Monitor.Enter(cs1);
			{
				DoWork();
				Thread.Sleep(3000);
				DoWork();
			}
			Monitor.Exit(cs1);
			DoWork();
		}
		Monitor.Exit(cs2);

		Console.WriteLine("Thread 2 has finished.");
	}
	
	public void Main() 
	{
		Thread t1 = new Thread(thread_proc_1);
		t1.Start();
		Thread.Sleep(1000);
		Thread t2 = new Thread(thread_proc_2);
		t2.Start();
		t1.Join();
		t2.Join();
	}
}