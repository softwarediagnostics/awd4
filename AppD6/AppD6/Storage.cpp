#include "stdafx.h"

static HANDLE hFileMap;

void AllocateStorage(volatile unsigned int **ppint)
{
	hFileMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(int), L"AppD6_SharedData");

	*ppint = (volatile unsigned int *)MapViewOfFileEx(hFileMap, FILE_MAP_WRITE, 0, 0, sizeof(int), (LPVOID)0x12340000);
}

void DeAllocateStorage(volatile unsigned int *pint)
{
	UnmapViewOfFile((LPCVOID)pint);
	CloseHandle(hFileMap);
}

