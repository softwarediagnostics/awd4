#include "stdafx.h"

DWORD WINAPI ThreadWorkProc(LPVOID);

void DoWork (void)
{
	Sleep(10);
	CreateThread(NULL, 0, ThreadWorkProc, NULL, 0, NULL);
}

DWORD WINAPI ThreadWorkProc(LPVOID)
{
	Sleep(10); // do some work
	return 0;
}
