// DllD2.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

extern "C"
{
__declspec(dllexport) void _cdecl Proc(LPVOID);
}

extern BYTE *lpData;

void Proc(LPVOID _lpData)
{
	lpData = (BYTE *)_lpData;
}

