// extended sample from https://github.com/microsoft/windows-rs/tree/master/crates/samples/windows/create_window

use windows::{
    core::*, Win32::Foundation::*, Win32::Graphics::Gdi::ValidateRect,
    Win32::System::LibraryLoader::GetModuleHandleW, Win32::UI::WindowsAndMessaging::*,
};

use std::thread;

const ID_START: usize = 1;

fn main() -> Result<()> {
    unsafe {
        let instance = GetModuleHandleW(None)?;
        debug_assert!(instance.0 != 0);

        let window_class = w!("AppR12");

        let wc = WNDCLASSW {
            hCursor: LoadCursorW(None, IDC_ARROW)?,
            hInstance: instance.into(),
            lpszClassName: window_class,
            style: CS_HREDRAW | CS_VREDRAW,
            lpfnWndProc: Some(wndproc),
            ..Default::default()
        };

        let atom = RegisterClassW(&wc);
        debug_assert!(atom != 0);

        let hwnd = CreateWindowExW(
            WINDOW_EX_STYLE::default(),
            window_class,
            w!("AppR12"),
            WS_OVERLAPPEDWINDOW | WS_VISIBLE,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            None,
            None,
            instance,
            None,
        );

        let hmenu = CreateMenu()?;
        let hfile_menu = CreateMenu()?;
        AppendMenuW(hfile_menu, MF_STRING, ID_START, w!("Start"))?;
        AppendMenuW(hmenu, MF_POPUP, hfile_menu.0 as usize, w!("File"))?;
        SetMenu(hwnd, hmenu)?;    

        let mut message = MSG::default();

        while GetMessageW(&mut message, None, 0, 0).into() {
            DispatchMessageW(&message);
        }

        Ok(())
    }
}

extern "system" fn wndproc(window: HWND, message: u32, wparam: WPARAM, lparam: LPARAM) -> LRESULT {
    unsafe {
        match message {
            WM_COMMAND => {
                match wparam {
                    WPARAM(ID_START) => { 
                        start_modeling();
                    }
                    _ => {}
                }
                LRESULT(0)
            }
            WM_PAINT => {
                ValidateRect(window, None);
                LRESULT(0)
            }
            WM_DESTROY => {
                PostQuitMessage(0);
                LRESULT(0)
            }
            _ => DefWindowProcW(window, message, wparam, lparam),
        }
    }
}

fn foo() {
    thread::sleep(std::time::Duration::from_secs(10));
    bar();
}

fn bar() {
    println!("Error");
}

fn start_modeling() {
    thread::spawn(|| { foo() }); 
}