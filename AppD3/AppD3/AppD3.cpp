// AppD3.cpp : Defines the entry point for the application.
//
// Accelerated Windows Debugging 3 training
// Copyright (c) 2013 - 2018 by Software Diagnostics Services
//

#include "stdafx.h"
#include "AppD3.h"

#include <string>
#include <vector>
#include <list>
#include <map>

using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void				StartModeling(void);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_APPD3, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_APPD3));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPD3));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_APPD3);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_START:
			StartModeling();
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

typedef struct MyDList
{
	double data;
	struct MyDList *pNext;
	struct MyDList *pPrev;
} MYDLIST, *PMYDLIST;

typedef struct MySubStruct
{
	vector<int> iVec;
	wstring wStr;
	list<string> sLst; 
	map<int, string> isMap;
} MYSUBSTRUCT;

typedef struct MyStruct
{
	LPSTR lpString;
	LPWSTR lpwString;
	DWORD dwArray[10];
	DWORD *pdwArray[10];
	DWORD **ppdwArray[10];
	DWORD dwArray2[10][10];
	MYSUBSTRUCT *pSubStruct;
	MYSUBSTRUCT subStruct;
	PMYDLIST pList;
} MYSTRUCT;

class MYCLASS 
{
public:
	MYCLASS(MYSTRUCT& _refMyStruct) : m_refMyStruct(_refMyStruct) {}

	virtual ~MYCLASS() {}
	virtual MYSTRUCT& getData() { return m_refMyStruct; } 

private:
	MYSTRUCT& m_refMyStruct;
};

class MYSUBCLASS : public MYCLASS
{
public:
	MYSUBCLASS(MYSTRUCT& _refMyStruct) : MYCLASS(_refMyStruct) { m_pszName = "Derived Class"; }

private:
	const char *m_pszName;
};

MYSTRUCT g_MyStruct; 
MYCLASS  g_MyClass(g_MyStruct);
MYSUBCLASS g_MySubClass(g_MyStruct); 

void InitializeMyStruct(MYSTRUCT& pms)
{
	for (int i = 0; i < sizeof(pms.dwArray) / sizeof(pms.dwArray[0]); ++i)
		pms.dwArray[i] = i;

	pms.lpString = "Hello World!";
	pms.lpwString = L"Hello World Wide!";
	
	for (int i = 0; i < sizeof(pms.pdwArray) / sizeof(pms.pdwArray[0]); ++i)
	{
		pms.pdwArray[i] = new DWORD; *pms.pdwArray[i] = i;
	}

	pms.pList = new MYDLIST;
	pms.pList->data = 1.1;
	pms.pList->pNext = new MYDLIST;
	pms.pList->pPrev = NULL;
	pms.pList->pNext->data = 2.2;
	pms.pList->pNext->pNext = new MYDLIST;
	pms.pList->pNext->pPrev = pms.pList->pNext; 
	pms.pList->pNext->pNext->data = 3.3;
	pms.pList->pNext->pNext->pNext = NULL;
	pms.pList->pNext->pNext->pPrev = pms.pList->pNext->pNext;

	for (int i = 0; i < sizeof(pms.ppdwArray) / sizeof(pms.ppdwArray[0]); ++i)
	{
		pms.ppdwArray[i] = new DWORD*[10]; 
		for (int j = 0; j < 10; ++j)
		{
			pms.ppdwArray[i][j] = new DWORD;
			*pms.ppdwArray[i][j] = j;
		}
	}

	for (int i = 0; i < sizeof(pms.dwArray2) / sizeof(pms.dwArray2[0]); ++i)
	{
		for (int j = 0; j < sizeof(pms.dwArray2[0]) / sizeof(pms.dwArray2[0][0]); ++j)
		{
			pms.dwArray2[i][j] = i*j;
		}
	}

	pms.pSubStruct = &pms.subStruct;

	pms.subStruct.iVec.push_back(1);  
	pms.subStruct.iVec.push_back(2);  
	pms.subStruct.iVec.push_back(3);  

	pms.subStruct.sLst.push_back("one");  
	pms.subStruct.sLst.push_back("two");  
	pms.subStruct.sLst.push_back("three");  

	pms.subStruct.wStr = L"MyWString";

	pms.subStruct.isMap[1] = "one";
	pms.subStruct.isMap[2] = "two";
	pms.subStruct.isMap[3] = "three";
}

void InitializeMyStruct(MYSTRUCT *pms)
{
	InitializeMyStruct(*pms);
}

void foo(MYSTRUCT&, MYSTRUCT *, const MYCLASS&, MYCLASS *, MYSUBCLASS&, MYSUBCLASS *);

void StartModeling(void)
{
	MYSTRUCT l_MyStruct; 
	MYSTRUCT *p_MyStruct = new MYSTRUCT;

	InitializeMyStruct(&g_MyStruct);
	InitializeMyStruct(&l_MyStruct);
	InitializeMyStruct(p_MyStruct);

	MYCLASS l_MyClass(l_MyStruct);
	MYCLASS *p_MyClass = new MYCLASS(*p_MyStruct);

	MYSUBCLASS l_MySubClass(l_MyStruct);
	MYSUBCLASS *p_MySubClass = new MYSUBCLASS(*p_MyStruct);

	foo(l_MyStruct, p_MyStruct, l_MyClass, p_MyClass, l_MySubClass, p_MySubClass);
}

void bar(int, LPSTR, LPWSTR);

void foo(MYSTRUCT& refStruct, MYSTRUCT * pStruct, const MYCLASS& refClass, MYCLASS * pClass, MYSUBCLASS& refSubClass, MYSUBCLASS * pSubClass)
{
	LPSTR lpString = pClass->getData().lpString; 
	LPWSTR lpwString = pSubClass->getData().lpwString;

	bar(1, lpString, lpwString);
}

void bar(int param1, LPSTR param2, LPWSTR param3)
{
	wchar_t local[1024] = L"Local wide character array";

	if (param1)
		bar(++param1, param2, param3);
}


